""":module test_seqio: Module that hosts all tests for seqio."""
import unittest
import sys
import os
import time

# Import the function to be tested.
sys.path.insert(0, '../seqio')
### FIXME: Replace PACKAGE, MODULE, and  FUNCTION such that `split_sequences`
### is imported.

from seqio.seqio import split_sequences

test_case = unittest.TestCase()

def test_file_notexist():
    """ Test that an IOError is thrown if the file does not exist. """
    status = False
    try:
        tmp = split_sequences("this_file_does_not_exist.fa")
    except IOError:
        ### FIXME: Change the next line such that the test passes if an
        ### IOError is thrown in case the file does not exist.
        status = True
    except:
        status = False

    # status should be true to make the test pass.
    test_case.assertTrue(status)

def test_returns():
    """ Test that the function returns the expected results for a test sequence file."""
    filename = "proteins.fasta"

    # Call the function.
    sequence_paths = split_sequences(filename)

    # Test the return type
    test_case.assertIsInstance(sequence_paths, dict)

    ### FIXME: Replace ### by appropriate dict methods to retrieve the keys and
    ### FiXME: values.
    # Get all keys and values from the returned dictionary and store in a sorted list.
    keys_as_list = sorted(sequence_paths.keys())
    values_as_list = sorted(sequence_paths.values())

    # Check that the correct keys are returned.
    assert keys_as_list == ["PFLU2282", "PFLU4500", "PFLU4601", "PFLU4699"]

    expected_paths = [os.path.abspath(os.path.join(os.getcwd(), k)+".fasta") for k in keys_as_list]
    assert values_as_list == expected_paths

    # These are the expected sequences
    expected_sequences = ["MSAIKLNVEGLNVRFFNGQKETHAVRDVSFALGREKLAIVGESGSGKSTVGRSLLKLHPPSARISAKAMAFGEVDLLSASEKAMQKIRGQRISMIMQDPKYSLNPVVKVGDQIAEAYLAHHKASRSEARERVMQMLEQVHIRDPKRVYNLYPHEVSGGMGQRIMIAMMVITRPEVIIADEPTSALDVSVRQQVLSVLEELVEQQQMGLIFVSHDLNLVRNYCDRVLVMYAGRVVESLAACDLQYAEHPYTRGLLAALPSMDNRRVHLPVLQRDPLWLTH",
                          "MPQTCDVVVLLSGTGSNLQALIDSTRTGDSPVRIAAVISNRSDAYGLQRARDAGIETRSLDHKTFDGREAFDSALIELIDAFNPKLVVLAGFMRILSADFVRHYEGRLLNIHPSLLPKYKGMHTHQRALDAGDSEHGCSVHFVTEELDGGPLVVQAVVPVESDDSAQSLAQRVHTQEHRIYPLAVRWFAEGRLILGDQGALLDGQLLAASGHLIRT",
                          "MLKSLDCPMSDRFELFLTCPKGLEGLLIEEAVGLGLEEAREHTSAVRGMADMETAYRLCLWSRLANRVLLVLKRFPMKDAEDLYHGVLDIEWADHMVPDGTLAVEFSGHGSGIDNTHFGALKVKDAIVDKLRTPAGERPSIDKINPDLRIHLRLDRGEAILSLDLSGHSLHQRGYRLQQGAAPLKENLAAAILIRSGWPRIAAEGGALTDPMCGVGTFLVEGAMIAADMAPNLNRELWGFTTWLGHVPALWKKLHTEATERAAIGMNKPPLWVRGYEADPRLIQPARNNIERAGLSHWIKVYQGEVGTFEPRPDQNQKGLVICNPPYGERLGDEASLLYLYQNLGERLRQACMGWEAAVFTGAPDLGKRMGIRSHKQYSFWNGALPCKLLLIKVNPDQFVTGERRTPEQRQAEREQAAYDQAPVEPQERQYNKNGNPIKPAPAPVVEQARLSEGGQMFANRLQKNLKLLGKWAKREGVDCYRVYDADMPEYSMAIDLYHDWVHVQEYAAPKSIDPEKASARMFDALAAIPQALNIDKSRVVVKRRERQSGTKQYERQSAQGKFTEVSEGGVKLLVNLTDYLDTGLFLDHRPMRMRIQKEAAGKRFLNLYCYTATASVHAAKGGARSTTSVDLSKTYLDWARRNFSLNGFSDKNRLEQGDVIAWLEASRDEFDLIFIDPPTFSNSKRMEGIFDVQRDHVQLLDLAMARLAPGGVLYFSNNFRKFVLEDNLSERYAVEEISDKTLDPDFARNAKIHRAWKITAR",
                          "VAEAYPWQDSLWQQLAGRAQHAHAYLLHGPVGIGKRDLAERLMASLLCQRPVNLEACGECKSCLLLKAGSHPDNYVLEPEEADKAIKVDQVRDLVSFVVQTAQMGGRKVVLIEPVEAMNINAANALLKSLEEPSGDTVLLLVSHQSSRLLPTIRSRCVQQACPLPSEAMSLEWLGKALPDCTEDERVELLTLAAGSPLAAVKLQAQGVREQRALVVDGVKKLIKQEMSATQLAETAWKDIPLLLLFDWFCDWSSLILRYQLTQDENGLGLPDMRKVVQYLAQKSAQDKVLTIQDWILAQRQKVLGKANLNRVLLLEALLVQWVGLLGRR"]

    # Loop over all filenames and expected sequence:
    for id, fname, expected_sequence in zip(keys_as_list, values_as_list, expected_sequences):
        with open(fname, "r") as file_handle:
            ### FIXME: Replace ### with a method to read in a line from an open file.
            line = file_handle.readline()[:-1] # Strip the '\n'.

            if line[0] == ">":
                ### FIXME: Replace ### such that the line starting at 2nd letter is compared to the seq. ID.
                test_case.assertEqual(line[1:], id)
            else:
                ### FIXME: Replace ### such that the entire line is compared to the expected sequence.
                test_case.assertEqual(line, expected_sequence)


def test_wrong_sequence_type():
    """ Test that a ValueError is thrown if the sequence type does not match the sequence."""

    raises_value_error = False
    # Call function, catch any exceptions.
    try:
        ### FIXME
        ### Call the `split_sequences()` function with arguments
        ### 'proteins.fasta' and "NA" to check the expected behaviour.
        ###
        split_sequences('proteins.fasta', alphabet="NA")
    except ValueError:
        # This is the expected behavior.
        raises_value_error = True
    except:
        # Unexpected behavior.
        raise

    # Assertion.
    test_case.assertTrue(raises_value_error)

def test_overwrite():
    """ Test that existing files are overwritten. """

    # Create dummy files.
    create_times  = {}
    for fname in ["PFLU2282.fasta", "PFLU4500.fasta", "PFLU4601.fasta", "PFLU4699.fasta"]:
        open(fname, 'w').close()
        ### FIXME: Store the create time in the dict `create_times`. Replace ###
        ### by the loop variable, such that it becomes the key of the dict.
        create_times[fname] = os.stat(fname).st_ctime

    time.sleep(1)
    # Call the function
    tmp = split_sequences("proteins.fasta")
    for fname in ["PFLU2282.fasta", "PFLU4500.fasta", "PFLU4601.fasta", "PFLU4699.fasta"]:
        ### FIXME: Replace ### by the create_times dict value for this file.
        test_case.assertGreater(os.stat(fname).st_ctime, create_times[fname])

