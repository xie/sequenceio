# SequenceIO

Template python project for test-driven development

## Contributors

| Name                   | email                                 |
|------------------------|---------------------------------------|
| Carsten Fortmann-Grote | carsten.fortmann-grote@evolbio.mpg.de |
| Neel Prabh             | prabh@evolbio.mpg.de                  |
| Max Raas               | raas@evolbio.mpg.de                   |
| Nadia Andreani         | andreani@evolbio.mpg.de               |
| Wagner Fagundes        | fagundes@evolbio.mpg.de               | 
| Charlotte Rossetti     | rossetti@evolbio.mpg.de               |
| Jennifer Yuzon         | yuzon@evolbio.mpg.de                  |
| Beatriz Mourato        | mourato@evolbio.mpg.de                |
| Natasha Puzovic        | puzovic@evolbio.mpg.de                |
| Chen Xie               | xie@evolbio.mpg.de                    |
